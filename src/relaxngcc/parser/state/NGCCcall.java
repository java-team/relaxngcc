/* this file is generated by RelaxNGCC */
package relaxngcc.parser.state;
import org.xml.sax.SAXException;
import org.xml.sax.Attributes;
import relaxngcc.parser.ParserRuntime;

    import relaxngcc.grammar.*;
    import relaxngcc.datatype.Datatype;
    import org.xml.sax.Locator;
  


class NGCCcall extends NGCCHandler {
    private String alias;
    private String withParams;
    protected final ParserRuntime $runtime;
    private int $_ngcc_current_state;
    protected String $uri;
    protected String $localName;
    protected String $qname;

    public final NGCCRuntime getRuntime() {
        return($runtime);
    }

    public NGCCcall(NGCCHandler parent, NGCCEventSource source, ParserRuntime runtime, int cookie) {
        super(source, parent, cookie);
        $runtime = runtime;
        $_ngcc_current_state = 2;
    }

    public NGCCcall(ParserRuntime runtime) {
        this(null, runtime, runtime, -1);
    }

    private void action0()throws SAXException {
        
      param = new NGCCCallParam($runtime, withParams, alias);
    
}

    public void enterElement(String $__uri, String $__local, String $__qname, Attributes $attrs) throws SAXException {
        int $ai;
        $uri = $__uri;
        $localName = $__local;
        $qname = $__qname;
        switch($_ngcc_current_state) {
        case 2:
            {
                if(($ai = $runtime.getAttributeIndex("http://www.xml.gr.jp/xmlns/relaxngcc","with-params"))>=0) {
                    $runtime.consumeAttribute($ai);
                    $runtime.sendEnterElement(super._cookie, $__uri, $__local, $__qname, $attrs);
                }
                else {
                    $_ngcc_current_state = 1;
                    $runtime.sendEnterElement(super._cookie, $__uri, $__local, $__qname, $attrs);
                }
            }
            break;
        case 1:
            {
                if(($ai = $runtime.getAttributeIndex("http://www.xml.gr.jp/xmlns/relaxngcc","alias"))>=0) {
                    NGCCHandler h = new NGCCalias(this, super._source, $runtime, 397);
                    spawnChildFromEnterElement(h, $__uri, $__local, $__qname, $attrs);
                }
                else {
                    NGCCHandler h = new NGCCalias(this, super._source, $runtime, 397);
                    spawnChildFromEnterElement(h, $__uri, $__local, $__qname, $attrs);
                }
            }
            break;
        case 0:
            {
                revertToParentFromEnterElement(param, super._cookie, $__uri, $__local, $__qname, $attrs);
            }
            break;
        default:
            {
                unexpectedEnterElement($__qname);
            }
            break;
        }
    }

    public void leaveElement(String $__uri, String $__local, String $__qname) throws SAXException {
        int $ai;
        $uri = $__uri;
        $localName = $__local;
        $qname = $__qname;
        switch($_ngcc_current_state) {
        case 2:
            {
                if(($ai = $runtime.getAttributeIndex("http://www.xml.gr.jp/xmlns/relaxngcc","with-params"))>=0) {
                    $runtime.consumeAttribute($ai);
                    $runtime.sendLeaveElement(super._cookie, $__uri, $__local, $__qname);
                }
                else {
                    $_ngcc_current_state = 1;
                    $runtime.sendLeaveElement(super._cookie, $__uri, $__local, $__qname);
                }
            }
            break;
        case 1:
            {
                if(($ai = $runtime.getAttributeIndex("http://www.xml.gr.jp/xmlns/relaxngcc","alias"))>=0) {
                    NGCCHandler h = new NGCCalias(this, super._source, $runtime, 397);
                    spawnChildFromLeaveElement(h, $__uri, $__local, $__qname);
                }
                else {
                    NGCCHandler h = new NGCCalias(this, super._source, $runtime, 397);
                    spawnChildFromLeaveElement(h, $__uri, $__local, $__qname);
                }
            }
            break;
        case 0:
            {
                revertToParentFromLeaveElement(param, super._cookie, $__uri, $__local, $__qname);
            }
            break;
        default:
            {
                unexpectedLeaveElement($__qname);
            }
            break;
        }
    }

    public void enterAttribute(String $__uri, String $__local, String $__qname) throws SAXException {
        int $ai;
        $uri = $__uri;
        $localName = $__local;
        $qname = $__qname;
        switch($_ngcc_current_state) {
        case 2:
            {
                if(($__uri.equals("http://www.xml.gr.jp/xmlns/relaxngcc") && $__local.equals("with-params"))) {
                    $_ngcc_current_state = 4;
                }
                else {
                    $_ngcc_current_state = 1;
                    $runtime.sendEnterAttribute(super._cookie, $__uri, $__local, $__qname);
                }
            }
            break;
        case 1:
            {
                if(($__uri.equals("http://www.xml.gr.jp/xmlns/relaxngcc") && $__local.equals("alias"))) {
                    NGCCHandler h = new NGCCalias(this, super._source, $runtime, 397);
                    spawnChildFromEnterAttribute(h, $__uri, $__local, $__qname);
                }
                else {
                    NGCCHandler h = new NGCCalias(this, super._source, $runtime, 397);
                    spawnChildFromEnterAttribute(h, $__uri, $__local, $__qname);
                }
            }
            break;
        case 0:
            {
                revertToParentFromEnterAttribute(param, super._cookie, $__uri, $__local, $__qname);
            }
            break;
        default:
            {
                unexpectedEnterAttribute($__qname);
            }
            break;
        }
    }

    public void leaveAttribute(String $__uri, String $__local, String $__qname) throws SAXException {
        int $ai;
        $uri = $__uri;
        $localName = $__local;
        $qname = $__qname;
        switch($_ngcc_current_state) {
        case 3:
            {
                if(($__uri.equals("http://www.xml.gr.jp/xmlns/relaxngcc") && $__local.equals("with-params"))) {
                    $_ngcc_current_state = 1;
                }
                else {
                    unexpectedLeaveAttribute($__qname);
                }
            }
            break;
        case 2:
            {
                $_ngcc_current_state = 1;
                $runtime.sendLeaveAttribute(super._cookie, $__uri, $__local, $__qname);
            }
            break;
        case 1:
            {
                NGCCHandler h = new NGCCalias(this, super._source, $runtime, 397);
                spawnChildFromLeaveAttribute(h, $__uri, $__local, $__qname);
            }
            break;
        case 0:
            {
                revertToParentFromLeaveAttribute(param, super._cookie, $__uri, $__local, $__qname);
            }
            break;
        default:
            {
                unexpectedLeaveAttribute($__qname);
            }
            break;
        }
    }

    public void text(String $value) throws SAXException {
        int $ai;
        switch($_ngcc_current_state) {
        case 2:
            {
                if(($ai = $runtime.getAttributeIndex("http://www.xml.gr.jp/xmlns/relaxngcc","with-params"))>=0) {
                    $runtime.consumeAttribute($ai);
                    $runtime.sendText(super._cookie, $value);
                }
                else {
                    $_ngcc_current_state = 1;
                    $runtime.sendText(super._cookie, $value);
                }
            }
            break;
        case 1:
            {
                if(($ai = $runtime.getAttributeIndex("http://www.xml.gr.jp/xmlns/relaxngcc","alias"))>=0) {
                    NGCCHandler h = new NGCCalias(this, super._source, $runtime, 397);
                    spawnChildFromText(h, $value);
                }
                else {
                    NGCCHandler h = new NGCCalias(this, super._source, $runtime, 397);
                    spawnChildFromText(h, $value);
                }
            }
            break;
        case 0:
            {
                revertToParentFromText(param, super._cookie, $value);
            }
            break;
        case 4:
            {
                withParams = $value;
                $_ngcc_current_state = 3;
            }
            break;
        }
    }

    public void onChildCompleted(Object $__result__, int $__cookie__, boolean $__needAttCheck__)throws SAXException {
        switch($__cookie__) {
        case 397:
            {
                alias = ((String)$__result__);
                action0();
                $_ngcc_current_state = 0;
            }
            break;
        }
    }

    public boolean accepted() {
        return(($_ngcc_current_state == 0));
    }

    
      NGCCCallParam param;
    
}

